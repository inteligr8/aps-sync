#!/bin/bash
THISDIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

GROUP_NAME=$2
if [[ -z $GROUP_NAME ]]; then
    echo "A group name is required"
    exit 1
fi

PERMISSION_ID=$3
if [[ -z $PERMISSION_ID ]]; then
    PERMISSION_ID=read
fi

. $THISDIR/aps-common.sh

APS_API_REST_URL=$APS_BASEURL/app/rest

CURL_JSON="$CURL -H \"Content-type: application/json\""

# Discover Tenant
TENANT_ID=$(eval $CURL_JSON -X GET $APS_API_URL/admin/tenants | jq '.[] | .id')
echo "Tenant: $TENANT_ID"

GROUPS_JSON=$(eval "$CURL_JSON -X GET $APS_API_URL/admin/groups?tenantId=$TENANT_ID\&functional=true")
GROUP_ID=$(eval "echo '$GROUPS_JSON' | jq '.[] | select(.name==\"$GROUP_NAME\") | .id'")
echo "Found Group ID: $GROUP_ID"

function shareModels() {
    local TARGET_MODEL_IDS=$1
    local GROUP_ID=$2
    local PERMISSION_ID=$3

    while IFS= read -r TARGET_MODEL_ID; do
        GROUP_SHARE_JSON=$(eval "$CURL_JSON -X GET $APS_API_REST_URL/models/$TARGET_MODEL_ID/share-info | jq '.data | select(. != null) | .[] | select(.group.id==$GROUP_ID)'")

        if [[ -z $GROUP_SHARE_JSON ]]; then
            echo "Sharing model: $TARGET_MODEL_ID"
            eval "$CURL_JSON -X PUT -d '{\"added\": [{\"groupId\": $GROUP_ID, \"permission\": \"$PERMISSION_ID\"}]}' $APS_API_REST_URL/models/$TARGET_MODEL_ID/share-info"
        else
            GROUP_SHARE_PERMISSION=$(eval "echo '$GROUP_SHARE_JSON' | jq -r '.permission'")
            if [[ "$GROUP_SHARE_PERMISSION" != "$PERMISSION_ID" ]]; then
                GROUP_SHARE_ID=$(eval "echo '$GROUP_SHARE_JSON' | jq -r '.id'")
                echo "Updating sharing of model: $TARGET_MODEL_ID"
                eval "$CURL_JSON -X PUT -d '{\"updated\": [{\"id\": $GROUP_SHARE_ID, \"permission\": \"$PERMISSION_ID\"}]}' $APS_API_REST_URL/models/$TARGET_MODEL_ID/share-info"
            fi
        fi
    done < <(printf '%s\n' "$TARGET_MODEL_IDS")
}

echo "Finding Processes in APS ..."
TARGET_PROC_IDS=$(eval "$CURL_JSON -X GET $APS_API_URL/models?modelType=0 | jq '.data[].id'")
if [[ ! -z $TARGET_PROC_IDS ]]; then
    shareModels "$TARGET_PROC_IDS" $GROUP_ID $PERMISSION_ID
fi

echo "Finding ?? in APS ..."
TARGET_XX_IDS=$(eval "$CURL_JSON -X GET $APS_API_URL/models?modelType=1 | jq '.data[].id'")
if [[ ! -z $TARGET_XX_IDS ]]; then
    shareModels "$TARGET_XX_IDS" $GROUP_ID $PERMISSION_ID
fi

echo "Finding Forms in APS ..."
TARGET_FORM_IDS=$(eval "$CURL_JSON -X GET $APS_API_URL/models?modelType=2 | jq '.data[].id'")
if [[ ! -z $TARGET_FORM_IDS ]]; then
    shareModels "$TARGET_FORM_IDS" $GROUP_ID $PERMISSION_ID
fi

echo "Finding Apps in APS ..."
TARGET_APP_IDS=$(eval "$CURL_JSON -X GET $APS_API_URL/models?modelType=3 | jq '.data[].id'")
if [[ ! -z $TARGET_APP_IDS ]]; then
    shareModels "$TARGET_APP_IDS" $GROUP_ID $PERMISSION_ID
fi

