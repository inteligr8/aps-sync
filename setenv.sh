
#DEBUG=1

if [[ -z $APS_BASEURL ]]; then
    APS_BASEURL=http://localhost:8080/activiti-app
fi


### HTTP BASIC authentication

if [[ -z $APS_USERNAME ]]; then
    APS_USERNAME=admin
fi

if [[ -z $APS_PASSWORD ]]; then
    APS_PASSWORD=admin
fi


### OAuth authentication

# leave unset for BASIC auth
# possible values: password
#OAUTH_AUTHTYPE=password

if [[ -z $OAUTH_BASEURL ]]; then
    OAUTH_BASEURL=http://auth.example.org:8080/auth
fi

if [[ -z $AIS_REALM ]]; then
    AIS_REALM=alfresco
fi

if [[ -z $AIS_TOKENURL ]]; then
    AIS_TOKENURL=$OAUTH_BASEURL/realms/$AIS_REALM/protocol/openid-connect/token
fi

if [[ -z $OAUTH_TOKENURL ]]; then
    OAUTH_TOKENURL=$AIS_TOKENURL
fi

if [[ -z $OAUTH_USERNAME ]]; then
    OAUTH_USERNAME=$APS_USERNAME
fi

if [[ -z $OAUTH_PASSWORD ]]; then
    OAUTH_PASSWORD=$APS_PASSWORD
fi

if [[ -z $OAUTH_CLIENT_ID ]]; then
    OAUTH_CLIENT_ID=alfresco
fi

if [[ -z $OAUTH_CLIENT_SECRET ]]; then
    OAUTH_CLIENT_SECRET=alfresco-secret
fi
