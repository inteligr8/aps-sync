#!/bin/bash
THISDIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

. $THISDIR/curl-common.sh

echo "Using APS base URL: $APS_BASEURL"

APS_API_URL=$APS_BASEURL/api/enterprise

SERVER_INFO_JSON=$(eval "$CURL -X GET $APS_API_URL/app-version")
if [[ "$SERVER_INFO_JSON" =~ Unauthorized ]]; then
	echo "Unauthorized"
	exit 1
fi

APS_VERSION=$(eval "echo '$SERVER_INFO_JSON' | jq -r '(.majorVersion + \".\" + .minorVersion + \".\" + .revisionVersion)'")
if [[ -z $APS_VERSION ]]; then
    echo "Unable to retrieve basic information from the server"
    exit 1
fi
echo "Found APS server with version: $APS_VERSION"

export APS_MAJOR_VERSION=$(eval "echo '$SERVER_INFO_JSON' | jq -r '.majorVersion'")
export APS_MINOR_VERSION=$(eval "echo '$SERVER_INFO_JSON' | jq -r '.minorVersion'")
export APS_REV_VERSION=$(eval "echo '$SERVER_INFO_JSON' | jq -r '.revisionVersion'")

export APS_API_URL
