#!/bin/bash
THISDIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

. $THISDIR/aps-common.sh

APS_APP_NAME=$1
if [[ -z $APS_APP_NAME ]]; then
    echo "An APS App name argument to this script is required"
    exit 1
fi

if [[ -z $APS_APPS_DIR ]]; then
    APS_APPS_DIR=$PWD
elif [[ ! -d "$APS_APPS_DIR" ]]; then
    echo "The 'APS_APPS_DIR' environment variable does not reference a directory: $APS_APPS_DIR"
    exit 1
fi

APPS_JSON=$(eval "$CURL -X GET $APS_API_URL/models?modelType=3\&filter=everyone | jq '.'")
APP_ID=$(eval "echo '$APPS_JSON' | jq '.data[] | select(.name==\"$APS_APP_NAME\") | .id'")
if [[ -z $APP_ID ]]; then
    echo "Failed to find APS App '$APS_APP_NAME'"
    echo "Possible APS Apps"
    echo ''"$APPS_JSON"'' | jq -r '.data[].name'
    exit 1
fi

echo "Found APS App '$APS_APP_NAME' with ID: $APP_ID"

APS_APP_DIR=$APS_APPS_DIR/$APS_APP_NAME
APS_APP_ZIP=$APS_APP_DIR.zip

eval "$CURL -o \"$APS_APP_ZIP\" -X GET $APS_API_URL/app-definitions/$APP_ID/export"
if [[ ! -f "$APS_APP_ZIP" ]]; then
    echo "Failed to download APS App '$APS_APP_NAME'"
    exit 1
fi

rm -rf "$APS_APP_DIR"
unzip -d "$APS_APP_DIR" "$APS_APP_ZIP"
rm "$APS_APP_ZIP"
