#!/bin/bash
THISDIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

. $THISDIR/aps-common.sh

APS_APP_NAME=$1
if [[ -z $APS_APP_NAME ]]; then
    echo "An APS App name argument to this script is required"
    exit 1
fi

if [[ -z $APS_APPS_DIR ]]; then
    APS_APPS_DIR=$PWD
fi

APS_APP_DIR=$APS_APPS_DIR/$APS_APP_NAME
if [[ ! -d "$APS_APP_DIR" ]]; then
    echo "The App directory '$APS_APP_DIR' does not exist; cannot proceed"
    exit 1
fi

CURL_JSON="$CURL -H \"Content-type: application/json\""

# FIXME this could be changed to find the (only) JSON file
if [[ ! -f "$APS_APP_DIR/$APS_APP_NAME.json" ]]; then
    echo "The App descriptor '$APS_APP_NAME.json' does not exist; cannot proceed"
    exit 1
fi

# for convenience when debuging, switch to 'cp'
CMD_MV=mv

echo "Finding core meta-data about all process models in APS ..."
TARGET_PROCMODELS_JSON_TRIM=$(eval "$CURL -X GET $APS_API_URL/models?modelType=0 | jq -c '.data |= (unique_by(.name) | .[] |= {\"id\", \"name\", \"version\"})'")

echo "Finding core meta-data about all form models in APS ..."
TARGET_FORMMODELS_JSON_TRIM=$(eval "$CURL -X GET $APS_API_URL/models?modelType=2 | jq -c '.data |= (unique_by(.name) | .[] |= {\"id\", \"name\", \"version\"})'")

echo "Finding core meta-data about all organizations in APS ..."
TENANT_ID=$(eval "$CURL -X GET $APS_API_URL/admin/tenants | jq '.[] | .id'")
TARGET_ORGS_JSON_TRIM=$(eval "$CURL -X GET $APS_API_URL/admin/groups?tenantId=$TENANT_ID\&functional=true\&summary=true | jq -c '.[] |= {\"id\", \"name\", \"parentGroupId\"}'")

echo "Cross-referencing form model files and APS form model meta-data ..."
if [[ -d "$APS_APP_DIR/form-models" ]]; then
    cd "$APS_APP_DIR/form-models"
    for FORMMODEL_JSON_FILE in *.json; do
        SOURCE_FORMMODEL_ID=$(eval "echo '$FORMMODEL_JSON_FILE' | sed 's~[^/]\+-\([0-9]\+\)\.json~\1~'")
        FORMMODEL_NAME=$(eval "echo '$FORMMODEL_JSON_FILE' | sed 's~\([^/]\+\)-[0-9]\+\.json~\1~'")
        TARGET_FORMMODEL_ID=$(eval "echo '$TARGET_FORMMODELS_JSON_TRIM' | jq '.data[] | select(.name==\"$FORMMODEL_NAME\") | .id'")

        if [[ ! -z $TARGET_FORMMODEL_ID ]]; then
            TARGET_FORMMODELS_JSON_TRIM=$(eval "echo '$TARGET_FORMMODELS_JSON_TRIM' | jq -c '(.data[] | select(.name==\"$FORMMODEL_NAME\").oldId) = $SOURCE_FORMMODEL_ID'")

            if [[ "$TARGET_FORMMODEL_ID" == "$SOURCE_FORMMODEL_ID" ]]; then
                echo "The form model '$FORMMODEL_NAME' file already has the target APS form model ID"
            else
                echo "Renaming form model '$FORMMODEL_NAME' files with target APS form model IDs ..."
                $CMD_MV "$FORMMODEL_JSON_FILE" "$APS_APP_DIR/form-models/$FORMMODEL_NAME-$TARGET_FORMMODEL_ID.json"
                FORMMODEL_JSON_FILE=$APS_APP_DIR/form-models/$FORMMODEL_NAME-$TARGET_FORMMODEL_ID.json

                if [[ -f "$APS_APP_DIR/form-models/$FORMMODEL_NAME-$SOURCE_FORMMODEL_ID.png" ]]; then
                    $CMD_MV "$APS_APP_DIR/form-models/$FORMMODEL_NAME-$SOURCE_FORMMODEL_ID.png" "$APS_APP_DIR/form-models/$FORMMODEL_NAME-$TARGET_FORMMODEL_ID.png"
                fi
            fi
        fi
    done
fi

echo "Cross-referencing process model files and APS process model meta-data ..."
cd "$APS_APP_DIR/bpmn-models"
for PROCMODEL_JSON_FILE in *.json; do
    SOURCE_PROCMODEL_ID=$(eval "echo '$PROCMODEL_JSON_FILE' | sed 's~[^/]\+-\([0-9]\+\)\.json~\1~'")
    PROCMODEL_NAME=$(eval "echo '$PROCMODEL_JSON_FILE' | sed 's~\([^/]\+\)-[0-9]\+\.json~\1~'")
    TARGET_PROCMODEL_ID=$(eval "echo '$TARGET_PROCMODELS_JSON_TRIM' | jq '.data[] | select(.name==\"$PROCMODEL_NAME\") | .id'")

    if [[ ! -z $TARGET_PROCMODEL_ID ]]; then
        TARGET_PROCMODELS_JSON_TRIM=$(eval "echo '$TARGET_PROCMODELS_JSON_TRIM' | jq -c '(.data[] | select(.name==\"$PROCMODEL_NAME\").oldId) = $SOURCE_PROCMODEL_ID'")

        if [[ "$TARGET_PROCMODEL_ID" == "$SOURCE_PROCMODEL_ID" ]]; then
            echo "The process model '$PROCMODEL_NAME' file already has the target APS process model ID"
        else
            echo "Renaming process model '$PROCMODEL_NAME' files with target APS process model IDs ..."
            $CMD_MV "$PROCMODEL_JSON_FILE" "$APS_APP_DIR/bpmn-models/$PROCMODEL_NAME-$TARGET_PROCMODEL_ID.json"
            PROCMODEL_JSON_FILE=$APS_APP_DIR/bpmn-models/$PROCMODEL_NAME-$TARGET_PROCMODEL_ID.json

            PROCMODEL_XML_FILE=$APS_APP_DIR/bpmn-models/$PROCMODEL_NAME-$SOURCE_PROCMODEL_ID.bpmn20.xml
            $CMD_MV "$PROCMODEL_XML_FILE" "$APS_APP_DIR/bpmn-models/$PROCMODEL_NAME-$TARGET_PROCMODEL_ID.bpmn20.xml"
            PROCMODEL_XML_FILE=$APS_APP_DIR/bpmn-models/$PROCMODEL_NAME-$TARGET_PROCMODEL_ID.bpmn20.xml

            if [[ -f "$APS_APP_DIR/bpmn-models/$PROCMODEL_NAME-$SOURCE_PROCMODEL_ID.png" ]]; then
                $CMD_MV "$APS_APP_DIR/bpmn-models/$PROCMODEL_NAME-$SOURCE_PROCMODEL_ID.png" "$APS_APP_DIR/bpmn-models/$PROCMODEL_NAME-$TARGET_PROCMODEL_ID.png"
            fi
        fi
    fi
done

echo "Cross-referencing subprocess model files and APS process model meta-data ..."
if [[ -d "$APS_APP_DIR/bpmn-subprocess-models" ]]; then
    cd "$APS_APP_DIR/bpmn-subprocess-models"
    for PROCMODEL_JSON_FILE in *.json; do
        SOURCE_PROCMODEL_ID=$(eval "echo '$PROCMODEL_JSON_FILE' | sed 's~[^/]\+-\([0-9]\+\)\.json~\1~'")
        PROCMODEL_NAME=$(eval "echo '$PROCMODEL_JSON_FILE' | sed 's~\([^/]\+\)-[0-9]\+\.json~\1~'")
        TARGET_PROCMODEL_ID=$(eval "echo '$TARGET_PROCMODELS_JSON_TRIM' | jq '.data[] | select(.name==\"$PROCMODEL_NAME\") | .id'")

        if [[ ! -z $TARGET_PROCMODEL_ID ]]; then
            TARGET_PROCMODELS_JSON_TRIM=$(eval "echo '$TARGET_PROCMODELS_JSON_TRIM' | jq -c '(.data[] | select(.name==\"$PROCMODEL_NAME\").oldId) = $SOURCE_PROCMODEL_ID'")

            if [[ "$TARGET_PROCMODEL_ID" == "$SOURCE_PROCMODEL_ID" ]]; then
                echo "The subprocess model '$PROCMODEL_NAME' file already has the target APS process model ID"
            else
                echo "Renaming subprocess model '$PROCMODEL_NAME' files with target APS process model IDs ..."
                $CMD_MV "$PROCMODEL_JSON_FILE" "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_NAME-$TARGET_PROCMODEL_ID.json"
                PROCMODEL_JSON_FILE=$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_NAME-$TARGET_PROCMODEL_ID.json

                if [[ -f "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_NAME-$SOURCE_PROCMODEL_ID.png" ]]; then
                    $CMD_MV "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_NAME-$SOURCE_PROCMODEL_ID.png" "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_NAME-$TARGET_PROCMODEL_ID.png"
                fi
            fi
        fi
    done
fi

TARGET_PROCMODELS_JSON_LINES=$(eval "echo '$TARGET_PROCMODELS_JSON_TRIM' | jq -c '.data[]'")
TARGET_FORMMODELS_JSON_LINES=$(eval "echo '$TARGET_FORMMODELS_JSON_TRIM' | jq -c '.data[]'")
TARGET_ORGS_JSON_LINES=$(eval "echo '$TARGET_ORGS_JSON_TRIM' | jq -c '.[]'")


### Update App Descriptor ###

# Looping through defined process models
echo -n "Preparing to normalize app descriptor file with target APS process model IDs "
ZIP_APP_DESC_JQ_REPLACE="(.definition.models[] | .version) = 0"
while IFS= read -r TARGET_PROCMODEL_JSON; do
    PROCMODEL_NAME=$(eval "echo '$TARGET_PROCMODEL_JSON' | jq -r '.name'")
    TARGET_PROCMODEL_ID=$(eval "echo '$TARGET_PROCMODEL_JSON' | jq '.id'")

    if [[ ! -z $TARGET_PROCMODEL_ID ]]; then
        # Preparing a complicated 'jq' for normalizing the App descriptor
        ZIP_APP_DESC_JQ_REPLACE="$ZIP_APP_DESC_JQ_REPLACE | (.definition.models[] | select(.name==\"$PROCMODEL_NAME\").id) = $TARGET_PROCMODEL_ID"
    fi
    echo -n "."
done < <(printf '%s\n' "$TARGET_PROCMODELS_JSON_LINES")
echo ""

ZIP_APP_DESC_JQ_REPLACE="$ZIP_APP_DESC_JQ_REPLACE | del(.definition.models[].lastUpdated)" 

echo "Normalizing app descriptor file with target APS process model IDs ..."
jq "$ZIP_APP_DESC_JQ_REPLACE" "$APS_APP_DIR/$APS_APP_NAME.json" > "$APS_APP_DIR/$APS_APP_NAME-procmodel.json"
mv "$APS_APP_DIR/$APS_APP_NAME-procmodel.json" "$APS_APP_DIR/$APS_APP_NAME.json"

# Looping through defined organizations
echo -n "Preparing to normalize app descriptor file with target APS organization IDs "
ZIP_APP_DESC_JQ_REPLACE="(.definition.publishIdentityInfo[].group | .parentGroupId) = 0"
while IFS= read -r TARGET_ORG_JSON; do
    ORG_NAME=$(eval "echo '$TARGET_ORG_JSON' | jq -r '.name'")
    TARGET_ORG_ID=$(eval "echo '$TARGET_ORG_JSON' | jq '.id'")
    #TARGET_PARENT_ORG_ID=$(eval "echo '$TARGET_ORG_JSON' | jq '.parentGroupId'")

    if [[ -z $TARGET_ORG_ID ]]; then
        echo "Organization not found but is required: $ORG_NAME"
        exit 1
    fi

    # Preparing a complicated 'jq' for normalizing the App descriptor
    ZIP_APP_DESC_JQ_REPLACE="$ZIP_APP_DESC_JQ_REPLACE | (.definition.publishIdentityInfo[].group | select(.name==\"$ORG_NAME\").id) = $TARGET_ORG_ID"
    echo -n "."
done < <(printf '%s\n' "$TARGET_ORGS_JSON_LINES")
echo ""

echo "Normalizing app descriptor file with target APS organization IDs ..."
jq "$ZIP_APP_DESC_JQ_REPLACE" "$APS_APP_DIR/$APS_APP_NAME.json" > "$APS_APP_DIR/$APS_APP_NAME-org.json"
mv "$APS_APP_DIR/$APS_APP_NAME-org.json" "$APS_APP_DIR/$APS_APP_NAME.json"



### Update Form Descriptors (nothing right now) ###

if [[ -d "$APS_APP_DIR/form-models" ]]; then
    cd "$APS_APP_DIR/form-models"
    for FORMMODEL_JSON_FILE in *.json; do
        TARGET_FORMMODEL_ID=$(eval "echo '$FORMMODEL_JSON_FILE' | sed 's~[^/]\+-\([0-9]\+\)\.json~\1~'")
        FORMMODEL_NAME=$(eval "echo '$FORMMODEL_JSON_FILE' | sed 's~\([^/]\+\)-[0-9]\+\.json~\1~'")

        if [[ -z $TARGET_FORMMODEL_ID ]]; then
            echo "The form model '$FORMMODEL_NAME' file does not exist in the target APS; no normalization required"
        # else
            # Normalizing process model JSON
            # TODO referenceId and formId are different; might not need to touch the referenceId
            # echo "Normalizing APS form model '$FORMMODEL_NAME' JSON with target APS form model IDs ..."
            # jq -c "(.referenceId = $TARGET_FORMMODEL_ID)" "$FORMMODEL_JSON_FILE" > "$APS_APP_DIR/form-models/tmp.json"
            # mv "$APS_APP_DIR/form-models/tmp.json" "$FORMMODEL_JSON_FILE"
        fi

        # more Git-friendly formatting
        echo "Beautifing form model '$FORMMODEL_NAME' JSON"
        jq '.' "$FORMMODEL_JSON_FILE" > "$FORMMODEL_JSON_FILE.new"
        mv "$FORMMODEL_JSON_FILE.new" "$FORMMODEL_JSON_FILE"
    done
    for FORMMODEL_PNG_FILE in *.png; do
        [ -f "$FORMMODEL_PNG_FILE" ] || continue
        rm "$FORMMODEL_PNG_FILE"
    done
fi



### Update Process Descriptors ###

SOURCE_GROUP_NAME_TO_ID_LINES=$(eval "sed -n 's/.*<modeler:group-info-name-\([0-9]\+\)><!\[CDATA\[\(.\+\)\]\]>.*/\2=\1/p' '$APS_APP_DIR/bpmn-models/'*.bpmn20.xml | awk '!seen[\$0]++'")

cd "$APS_APP_DIR/bpmn-models"
for PROCMODEL_JSON_FILE in *.json; do
    TARGET_PROCMODEL_ID=$(eval "echo '$PROCMODEL_JSON_FILE' | sed 's~[^/]\+-\([0-9]\+\)\.json~\1~'")
    PROCMODEL_NAME=$(eval "echo '$PROCMODEL_JSON_FILE' | sed 's~\([^/]\+\)-[0-9]\+\.json~\1~'")

    PROCMODEL_XML_FILE=$PROCMODEL_NAME-$TARGET_PROCMODEL_ID.bpmn20.xml

    if [[ -z $TARGET_PROCMODEL_ID ]]; then
        echo "The process model '$PROCMODEL_NAME' does not exist in APS; its own ID doesn't need normalized"
        # FIXME maybe normalize to 0, just in case the ID actually matches something unrelated?
    else
        # Normalizing process model BPMN XML
        echo "Normalizing process model '$PROCMODEL_NAME' BPMN XML with target APS process model IDs ..."
        sed -i 's/modeler:modelId="[0-9]\+"/modeler:modelId="'$TARGET_PROCMODEL_ID'"/' "$APS_APP_DIR/bpmn-models/$PROCMODEL_XML_FILE"
        sed -i 's/modeler:modelVersion="[0-9]\+"/modeler:modelVersion="0"/' "$APS_APP_DIR/bpmn-models/$PROCMODEL_XML_FILE"
        sed -i 's/modeler:exportDateTime="[0-9]\+"//' "$APS_APP_DIR/bpmn-models/$PROCMODEL_XML_FILE"
        sed -i 's/modeler:modelLastUpdated="[0-9]\+"//' "$APS_APP_DIR/bpmn-models/$PROCMODEL_XML_FILE"

        # Normalizing process model JSON
        echo "Normalizing process model '$PROCMODEL_NAME' JSON with target APS process model IDs ..."
        jq -c "(.resourceId = $TARGET_PROCMODEL_ID)" "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE" > "$APS_APP_DIR/bpmn-models/tmp.json"
        mv "$APS_APP_DIR/bpmn-models/tmp.json" "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE"
    fi

    # Looping through defined organizations
    echo "Normalizing process model '$PROCMODEL_NAME' BPMN XML and JSON with target APS organizations ..."
    if [[ ! -z "$SOURCE_GROUP_NAME_TO_ID_LINES" ]]; then
        while IFS= read -r SOURCE_GROUP_NAME_TO_ID; do
            ORG_NAME=$(eval "echo '$SOURCE_GROUP_NAME_TO_ID' | sed -n 's/\(.\+\)=[0-9]\+/\1/p'")
            SOURCE_ORG_ID=$(eval "echo '$SOURCE_GROUP_NAME_TO_ID' | sed -n 's/.*=\([0-9]\+\)/\1/p'")
            TARGET_ORG_ID=$(eval "echo '$TARGET_ORGS_JSON_TRIM' | jq '.[] | select(.name==\"$ORG_NAME\") | .id'")

            if [[ -z $TARGET_ORG_ID ]]; then
                echo "The organization '$ORG_NAME' does not exist in APS"
                exit 1
            fi
            
            if [[ -v DEBUG ]]; then echo "Translating '$ORG_NAME' from '$SOURCE_ORG_ID' to '$TARGET_ORG_ID'"; fi

            # BPMN XML
            if [[ -v DEBUG ]]; then echo "Normalizing process model '$PROCMODEL_NAME' BPMN XML against target APS organization '$ORG_NAME'"; fi
            sed -i 's/activiti:candidateGroups="'$SOURCE_ORG_ID'"/activiti:candidateGroups="'$TARGET_ORG_ID'"/g' "$APS_APP_DIR/bpmn-models/$PROCMODEL_XML_FILE"
            sed -i 's/<\(\/\)\?modeler:group-info-name-'$SOURCE_ORG_ID'>/<\1modeler:group-info-name-'$TARGET_ORG_ID'>/g' "$APS_APP_DIR/bpmn-models/$PROCMODEL_XML_FILE"

            # JSON
            if [[ -v DEBUG ]]; then echo "Normalizing process model '$PROCMODEL_NAME' JSON against target APS organization '$ORG_NAME'"; fi
            jq -c "(.childShapes[].properties.usertaskassignment | select(type == \"object\").assignment.idm.candidateGroups[] | select(.name==\"$ORG_NAME\").id) = $TARGET_ORG_ID" "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE" > "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE-org"
            mv "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE-org" "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE"
            
            if [[ ! -v DEBUG ]]; then echo -n "."; fi
        done < <(printf '%s\n' "$SOURCE_GROUP_NAME_TO_ID_LINES")
        echo ""
    fi

    # Looping through defined form models
    echo "Normalizing process model '$PROCMODEL_NAME' BPMN XML and JSON with target APS form models ..."
    if [[ ! -z "$TARGET_FORMMODELS_JSON_LINES" ]]; then
        while IFS= read -r TARGET_FORMMODEL_JSON; do
            SOURCE_FORMMODEL_ID=$(eval "echo '$TARGET_FORMMODEL_JSON' | jq '.oldId'")
            FORMMODEL_NAME=$(eval "echo '$TARGET_FORMMODEL_JSON' | jq -r '.name'")
            TARGET_FORMMODEL_ID=$(eval "echo '$TARGET_FORMMODEL_JSON' | jq '.id'")

            if [[ -z $TARGET_FORMMODEL_ID ]]; then
                echo "The form '$FORMMODEL_NAME' does not exist in APS; no normalization needed"
            elif [[ -z $SOURCE_FORMMODEL_ID ]]; then
                if [[ -v DEBUG ]]; then echo "The process '$FORMMODEL_NAME' is not in this APS App; ignoring ..."; fi
            else
                if [[ -v DEBUG ]]; then echo "Translating '$FORMMODEL_NAME' from '$SOURCE_FORMMODEL_ID' to '$TARGET_FORMMODEL_ID'"; fi

                # BPMN XML
                if [[ -v DEBUG ]]; then echo "Normalizing process model '$PROCMODEL_NAME' BPMN XML against target APS form '$FORMMODEL_NAME'"; fi
                sed -i 's/activiti:formKey="'$SOURCE_FORMMODEL_ID'"/activiti:formKey="'$TARGET_FORMMODEL_ID'"/g' "$APS_APP_DIR/bpmn-models/$PROCMODEL_XML_FILE"
                sed -i 's/modeler:outcomeFormId="'$SOURCE_FORMMODEL_ID'"/modeler:outcomeFormId="'$TARGET_FORMMODEL_ID'"/g' "$APS_APP_DIR/bpmn-models/$PROCMODEL_XML_FILE"
                sed -i "s/form${SOURCE_FORMMODEL_ID}outcome/form${TARGET_FORMMODEL_ID}outcome/g" "$APS_APP_DIR/bpmn-models/$PROCMODEL_XML_FILE"
                eval "sed -i -e '1h;2,\$H;\$!d;g' -e 's/\(<!\[CDATA\[\)$SOURCE_FORMMODEL_ID\(\]\]><\/modeler:form-reference-id>\W*<modeler:form-reference-name><!\[CDATA\[\)[A-Za-z0-9\-\W ]\+\(\]\]><\/modeler:form-reference-name>\)/\1$TARGET_FORMMODEL_ID\2$FORMMODEL_NAME\3/g' '$APS_APP_DIR/bpmn-models/$PROCMODEL_XML_FILE'"

                # JSON
                if [[ -v DEBUG ]]; then echo "Normalizing process model '$PROCMODEL_NAME' JSON against target APS form '$FORMMODEL_NAME'"; fi
                jq -c "(.childShapes[].properties.conditionsequenceflow | select(. != null) | select(type == \"object\").expression | select(. != null) | select(.outcomeFormName==\"$FORMMODEL_NAME\").outcomeFormId) = \"$TARGET_FORMMODEL_ID\"" "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE" > "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE-form"
                jq -c "(.childShapes[].properties.conditionsequenceflow | select(. != null) | select(type == \"object\").expression | select(. != null) | select(.rightOutcomeFormName==\"$FORMMODEL_NAME\").rightOutcomeFormId) = \"$TARGET_FORMMODEL_ID\"" "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE" > "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE-form"
                mv "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE-form" "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE"
                jq -c "(.childShapes[].properties.formreference | select(. != null) | select(type == \"object\") | select(.name==\"$FORMMODEL_NAME\").id) = $TARGET_FORMMODEL_ID" "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE" > "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE-form"
                mv "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE-form" "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE"
            
                if [[ ! -v DEBUG ]]; then echo -n "."; fi
            fi
        done < <(printf '%s\n' "$TARGET_FORMMODELS_JSON_LINES")
        echo ""
    fi

    # Looping through defined (sub)process models
    echo "Normalizing process model '$PROCMODEL_NAME' BPMN XML and JSON with target APS subprocess models ..."
    if [[ ! -z "$TARGET_PROCMODELS_JSON_LINES" ]]; then
        while IFS= read -r TARGET_SUBPROCMODEL_JSON; do
            SOURCE_SUBPROCMODEL_ID=$(eval "echo '$TARGET_SUBPROCMODEL_JSON' | jq '.oldId'")
            SUBPROCMODEL_NAME=$(eval "echo '$TARGET_SUBPROCMODEL_JSON' | jq -r '.name'")
            TARGET_SUBPROCMODEL_ID=$(eval "echo '$TARGET_SUBPROCMODEL_JSON' | jq '.id'")

            if [[ -z $TARGET_SUBPROCMODEL_ID ]]; then
                echo "The process '$SUBPROCMODEL_NAME' does not exist in APS; no normalization needed"
            elif [[ -z $SOURCE_SUBPROCMODEL_ID ]]; then
                if [[ -v DEBUG ]]; then echo "The process '$SUBPROCMODEL_NAME' is not in this APS App; ignoring ..."; fi
            else
                if [[ -v DEBUG ]]; then echo "Translating '$SUBPROCMODEL_NAME' from '$SOURCE_SUBPROCMODEL_ID' to '$TARGET_SUBPROCMODEL_ID'"; fi

                # BPMN XML
                if [[ -v DEBUG ]]; then echo "Normalizing process model '$PROCMODEL_NAME' BPMN XML against target APS subprocess '$SUBPROCMODEL_NAME'"; fi
                eval "sed -i -e '1h;2,\$H;\$!d;g' -e 's/\(<!\[CDATA\[\)$SOURCE_SUBPROCMODEL_ID\(\]\]><\/modeler:subprocess-id>\W*<modeler:subprocess-name><!\[CDATA\[\)[A-Za-z0-9\-\W ]\+\(\]\]><\/modeler:subprocess-name>\)/\1$TARGET_SUBPROCMODEL_ID\2$SUBPROCMODEL_NAME\3/g' '$APS_APP_DIR/bpmn-models/$PROCMODEL_XML_FILE'"

                # JSON
                if [[ -v DEBUG ]]; then echo "Normalizing process model '$PROCMODEL_NAME' JSON against target APS subprocess '$SUBPROCMODEL_NAME'"; fi
                jq -c "(.childShapes[].properties.subprocessreference | select(. != null) | select(type == \"object\") | select(.name==\"$SUBPROCMODEL_NAME\").id) = $TARGET_SUBPROCMODEL_ID" "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE" > "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE-subproc"
                mv "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE-subproc" "$APS_APP_DIR/bpmn-models/$PROCMODEL_JSON_FILE"

                if [[ ! -v DEBUG ]]; then echo -n "."; fi
            fi
        done < <(printf '%s\n' "$TARGET_PROCMODELS_JSON_LINES")
        echo ""
    fi
    
    # more Git-friendly formatting
    echo "Beautifing process model '$PROCMODEL_NAME' JSON"
    jq '.' "$PROCMODEL_JSON_FILE" > "$PROCMODEL_JSON_FILE.new"
    mv "$PROCMODEL_JSON_FILE.new" "$PROCMODEL_JSON_FILE"
done
for PROCMODEL_PNG_FILE in *.png; do
    [ -f "$PROCMODEL_PNG_FILE" ] || continue
    rm "$PROCMODEL_PNG_FILE"
done


### Update Sub-Process Descriptors ###

if [[ -d "$APS_APP_DIR/bpmn-subprocess-models" ]]; then
    cd "$APS_APP_DIR/bpmn-subprocess-models"
    for PROCMODEL_JSON_FILE in *.json; do
        TARGET_PROCMODEL_ID=$(eval "echo '$PROCMODEL_JSON_FILE' | sed 's~[^/]\+-\([0-9]\+\)\.json~\1~'")
        PROCMODEL_NAME=$(eval "echo '$PROCMODEL_JSON_FILE' | sed 's~\([^/]\+\)-[0-9]\+\.json~\1~'")

        if [[ -z $TARGET_PROCMODEL_ID ]]; then
            echo "The subprocess model '$PROCMODEL_NAME' does not exist in the target APS; no noramlization required"
        else
            # Normalizing subprocess model JSON
            echo "Normalizing subprocess model '$PROCMODEL_NAME' JSON with target APS process model IDs ..."
            eval "jq -c '(.editorJson.resourceId = $TARGET_PROCMODEL_ID)' '$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE' > '$APS_APP_DIR/bpmn-subprocess-models/tmp.json'"
            mv "$APS_APP_DIR/bpmn-subprocess-models/tmp.json" "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE"
        fi

        # Looping through defined organizations
        echo "Normalizing subprocess model '$PROCMODEL_NAME' JSON with target APS organizations ..."
        if [[ ! -z "$SOURCE_GROUP_NAME_TO_ID_LINES" ]]; then
            while IFS= read -r SOURCE_GROUP_NAME_TO_ID; do
                ORG_NAME=$(eval "echo '$SOURCE_GROUP_NAME_TO_ID' | sed -n 's/\(.\+\)=[0-9]\+/\1/p'")
                TARGET_ORG_ID=$(eval "echo '$TARGET_ORGS_JSON_TRIM' | jq '.[] | select(.name==\"$ORG_NAME\") | .id'")

                # JSON
                if [[ -v DEBUG ]]; then echo "Normalizing subprocess model '$PROCMODEL_NAME' JSON against target APS organization '$ORG_NAME'"; fi
                jq -c "(.editorJson.childShapes[].properties.usertaskassignment | select(type == \"object\").assignment.idm.candidateGroups[] | select(.name==\"$ORG_NAME\").id) = $TARGET_ORG_ID" "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE" > "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE-org"
                mv "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE-org" "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE"

                if [[ ! -v DEBUG ]]; then echo -n "."; fi
            done < <(printf '%s\n' "$SOURCE_GROUP_NAME_TO_ID_LINES")
            echo ""
        fi

        # Looping through defined form models
        echo "Normalizing subprocess model '$PROCMODEL_NAME' JSON with target APS form models ..."
        if [[ ! -z "$TARGET_FORMMODELS_JSON_LINES" ]]; then
            while IFS= read -r TARGET_FORMMODEL_JSON; do
                FORMMODEL_NAME=$(eval "echo '$TARGET_FORMMODEL_JSON' | jq -r '.name'")
                TARGET_FORMMODEL_ID=$(eval "echo '$TARGET_FORMMODEL_JSON' | jq '.id'")

                # JSON
                if [[ -v DEBUG ]]; then echo "Normalizing subprocess model '$PROCMODEL_NAME' JSON against target APS form '$FORMMODEL_NAME'"; fi
                jq -c "(.editorJson.childShapes[].properties.conditionsequenceflow | select(. != null) | select(type == \"object\").expression | select(. != null) | select(.outcomeFormName==\"$FORMMODEL_NAME\").outcomeFormId) = \"$TARGET_FORMMODEL_ID\"" "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE" > "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE-form"
                jq -c "(.editorJson.childShapes[].properties.conditionsequenceflow | select(. != null) | select(type == \"object\").expression | select(. != null) | select(.rightOutcomeFormName==\"$FORMMODEL_NAME\").rightOutcomeFormId) = \"$TARGET_FORMMODEL_ID\"" "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE" > "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE-form"
                mv "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE-form" "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE"
                jq -c "(.editorJson.childShapes[].properties.formreference | select(. != null) | select(type == \"object\") | select(.name==\"$FORMMODEL_NAME\").id) = $TARGET_FORMMODEL_ID" "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE" > "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE-form"
                mv "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE-form" "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE"

                if [[ ! -v DEBUG ]]; then echo -n "."; fi
            done < <(printf '%s\n' "$TARGET_FORMMODELS_JSON_LINES")
            echo ""
        fi

        # Looping through defined (sub)process models
        echo "Normalizing subprocess model '$PROCMODEL_NAME' JSON with target APS subprocess models ..."
        while IFS= read -r TARGET_SUBPROCMODEL_JSON; do
            SUBPROCMODEL_NAME=$(eval "echo '$TARGET_SUBPROCMODEL_JSON' | jq -r '.name'")
            TARGET_SUBPROCMODEL_ID=$(eval "echo '$TARGET_SUBPROCMODEL_JSON' | jq '.id'")

            # JSON
            if [[ -v DEBUG ]]; then echo "Normalizing process model '$PROCMODEL_NAME' JSON against target APS subprocess '$SUBPROCMODEL_NAME'"; fi
            jq -c "(.editorJson.childShapes[].properties.subprocessreference | select(. != null) | select(type == \"object\") | select(.name==\"$SUBPROCMODEL_NAME\").id) = $TARGET_SUBPROCMODEL_ID" "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE" > "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE-subproc"
            mv "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE-subproc" "$APS_APP_DIR/bpmn-subprocess-models/$PROCMODEL_JSON_FILE"

            if [[ ! -v DEBUG ]]; then echo -n "."; fi
        done < <(printf '%s\n' "$TARGET_PROCMODELS_JSON_LINES")
        echo ""
        
        # more Git-friendly formatting
        echo "Beautifing subprocess model '$PROCMODEL_NAME' JSON"
        jq '.' "$PROCMODEL_JSON_FILE" > "$PROCMODEL_JSON_FILE.new"
        mv "$PROCMODEL_JSON_FILE.new" "$PROCMODEL_JSON_FILE"
    done
    for PROCMODEL_PNG_FILE in *.png; do
        [ -f "$PROCMODEL_PNG_FILE" ] || continue
        rm "$PROCMODEL_PNG_FILE"
    done
fi
