#!/bin/bash
THISDIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

. $THISDIR/aps-common.sh

APS_APP_NAME=$1
if [[ -z $APS_APP_NAME ]]; then
    echo "An APS App name argument to this script is required"
    exit 1
fi

if [[ -z $APS_APPS_DIR ]]; then
    APS_APPS_DIR=$PWD
fi

APS_APP_DIR=$APS_APPS_DIR/$APS_APP_NAME
APS_APP_ZIP=$APS_APP_DIR.zip

if [[ -f "$APS_APP_ZIP" ]]; then
    echo "Using already packed App '$APS_APP_ZIP' ..."
elif [[ -d "$APS_APP_DIR" ]]; then
    echo "Packing App '$APS_APP_NAME' ..."
    eval "cd '$APS_APP_DIR'; zip -r '$APS_APP_ZIP' *; cd '$THISDIR'"
    PACKED=true
else
    echo "The App directory '$APS_APP_DIR' or packed file '$APS_APP_ZIP' does not exist; cannot proceed"
    exit 1
fi

CURL_JSON="$CURL -H \"Content-type: application/json\""
CURL_MPFD="$CURL -H \"Content-type: multipart/form-data\""

echo "Finding App '$APS_APP_NAME' in APS ..."
TARGET_APP_ID=$(eval "$CURL -X GET $APS_API_URL/models?modelType=3 | jq '.data[] | select(.name==\"$APS_APP_NAME\") | .id'")

if [[ -z $TARGET_APP_ID ]]; then
    echo "APS App '$APS_APP_NAME' not found; creating a new one"

    echo "Importing App '$APS_APP_NAME' into APS ..."
    TMP=$(eval "$CURL_MPFD -X POST -F renewIdmEntries=true -F \"file=@$APS_APP_ZIP\" $APS_API_URL/app-definitions/import")
else
    echo "Found App '$APS_APP_NAME' with APS App ID: $TARGET_APP_ID"

    echo "Versioning App '$APS_APP_NAME' into APS ..."
    TMP=$(eval "$CURL_MPFD -X POST -F \"file=@$APS_APP_ZIP\" $APS_API_URL/app-definitions/$TARGET_APP_ID/import")
fi

if [[ -v $APP_PUBLISH ]]; then
    echo "Publishing App '$APS_APP_NAME' in APS ..."
    eval "$CURL_JSON -X POST -d \"{'comment': 'Published automatically by script'}\" $APS_API_URL/app-definitions/$TARGET_APP_ID/publish"
fi

if [[ ! -z $PACKED ]]; then
    eval "rm '$APS_APP_ZIP'"
fi
