#!/bin/bash

CMD=$1
BASEURL=$2
TYPE_JSON="-H 'Content-Type: application/json'"

# Discover Tenant
TENANT_ID=$(eval $CMD -X GET $BASEURL/admin/tenants | jq '.[] | .id')
echo "Tenant: $TENANT_ID"

JSON_CAP="\"type\": 0, \"tenantId\": $TENANT_ID"
JSON_ORG="\"type\": 1, \"tenantId\": $TENANT_ID"

GROUPSURL=$BASEURL/admin/groups

function cacheGroupMetadata() {
    GROUPS_JSON=$(eval "$CMD -X GET $GROUPSURL?tenantId=$TENANT_ID\&functional=true")
}

function createCapability() {
    local GROUP_NAME="$1"
    local JSON_CAPARRAY="$2"

    local GROUP_ID=$(eval "echo '$GROUPS_JSON' | jq '.[] | select(.name==\"$GROUP_NAME\") | .id'")
    if [[ -z $GROUP_ID ]]; then
        local GROUP_ID=$(eval "$CMD -X POST $TYPE_JSON -d '{\"name\": \"$GROUP_NAME\", $JSON_CAP}' $GROUPSURL | jq '.id'")
        eval "$CMD -X POST $TYPE_JSON -d '{\"capabilities\": $JSON_CAPARRAY}' $GROUPSURL/$GROUP_ID/capabilities"
    fi

    echo "$GROUP_ID"
}

function createOrganization() {
    local GROUP_NAME="$1"
    local PARENT_GROUP_ID=$2

    local GROUP_ID=$(eval "echo '$GROUPS_JSON' | jq '.[] | select(.name==\"$GROUP_NAME\") | .id'")
    if [[ -z $GROUP_ID ]]; then
        if [[ -z $PARENT_GROUP_ID ]]; then
            local GROUP_ID=$(eval "$CMD -X POST $TYPE_JSON -d '{\"name\": \"$GROUP_NAME\", $JSON_ORG}' $GROUPSURL | jq '.id'")
        else
            local GROUP_ID=$(eval "$CMD -X POST $TYPE_JSON -d '{\"name\": \"$GROUP_NAME\", \"parentGroupId\": $PARENT_GROUP_ID, $JSON_ORG}' $GROUPSURL | jq '.id'")
        fi
    elif [[ "$PARENT_GROUP_ID" != "" ]]; then
        linkParentToOrganizationById $GROUP_ID $PARENT_GROUP_ID
    fi

    echo "$GROUP_ID"
}

function linkCapabilityAndOrganization() {
    local CAP_ID=$1
    local ORG_ID=$2
    TMP=$(eval "$CMD -X POST $TYPE_JSON $GROUPSURL/$CAP_ID/related-groups/$ORG_ID?type=1")
}

function linkParentToOrganizationById() {
    local ORG_ID=$1
    local ORG_PARENT_ID=$2
    TMP=$(eval "$CMD -X PUT $TYPE_JSON -d '{\"parentGroupId\": $ORG_PARENT_ID}' $GROUPSURL/$ORG_ID")
}

function linkParentToOrganizationByName() {
    local ORG_NAME="$1"
    local ORG_PARENT_ID=$2

    local ORG_ID=$(eval "echo '$GROUPS_JSON' | jq '.[] | select(.name==\"$ORG_NAME\") | .id'")
    if [[ ! -z $ORG_ID ]]; then
        linkParentToOrganizationById $ORG_ID $ORG_PARENT_ID
    else
        echo "Organization \"$ORG_NAME\" does not exist; skipping heirarchical arrangement"
    fi
}
