
# APS Synchronizing Scripts

This project aims to provide a means to migrate models from one environment to another.  APS does not provide a good means for this, as IDs are different between environments.

## Support

I do not yet have the incentive to have this work with multiple versions of APS.  The exports do change between even minor versions of APS.  For example, `resource` was changed to `resourceId` between APS v1.11.0 and v1.11.4.  So these scripts will not work with v1.11.0 anymore.

Current version support: APS v1.11.4
