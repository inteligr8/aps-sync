#!/bin/bash
THISDIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

SOURCE_ENV_NAME=$1
shift
if [[ -z $SOURCE_ENV_NAME ]]; then
    echo "3 arguments are required: source environment name, target environment name, and the APS App name"
    exit 1
fi

TARGET_ENV_NAME=$1
shift
if [[ -z $TARGET_ENV_NAME ]]; then
    echo "2 more arguments are required: target environment name and the APS App name"
    exit 1
fi

eval "$THISDIR/aps-app-export.sh \"$SOURCE_ENV_NAME\" \"$@\""
if [[ $? != 0 ]]; then
    exit 1
fi

REFERENCE_ENV_NAME=$2
if [[ -z $REFERENCE_ENV_NAME ]]; then
    echo "A 4th argument is optional: reference environment name for normalization; not normalizing"
else
    eval "$THISDIR/aps-app-normalize.sh \"$REFERENCE_ENV_NAME\" \"$@\""
    if [[ $? != 0 ]]; then
        exit 1
    fi
fi

eval "$THISDIR/aps-app-import.sh \"$TARGET_ENV_NAME\" \"$@\""
