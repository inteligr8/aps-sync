#!/bin/bash
THISDIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [[ ! -z $1 ]]; then
    if [[ -f "$PWD/setenv-$1.sh" ]]; then
        . "$PWD/setenv-$1.sh"
        shift
    elif [[ -f "$THISDIR/setenv-$1.sh" ]]; then
        . "$THISDIR/setenv-$1.sh"
        shift
    else
        echo "A 'setenv-$1.sh' file is expected to exist; ignoring ..."
    fi
fi

if [[ -f "$PWD/setenv.sh" ]]; then
    . "$PWD/setenv.sh"
fi
if [[ -f "$THISDIR/setenv.sh" ]]; then
    . "$THISDIR/setenv.sh"
fi
