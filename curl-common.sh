#!/bin/bash
THISDIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

. $THISDIR/lib-env.sh

CURL="curl"
if [[ -z $DEBUG ]]; then
    CURL="$CURL -s"
else
    CURL="$CURL -v"
fi

if [[ -z $OAUTH_AUTHTYPE ]]; then
    echo "Authenticating with BASIC ..."
    CURL="$CURL -u \"$APS_USERNAME:$APS_PASSWORD\""
elif [[ "$OAUTH_AUTHTYPE" == "password" ]]; then
    FORM_PARAMS="-d grant_type=password -d username='$OAUTH_USERNAME' -d password='$OAUTH_PASSWORD' -d client_id=$OAUTH_CLIENT_ID"
    if [[ ! -z $OAUTH_CLIENT_SECRET ]]; then
        FORM_PARAMS="${FORM_PARAMS} -d client_secret='$OAUTH_CLIENT_SECRET'"
    fi
    
    echo "Authenticating with OIDC Password Grant ..."
    OAUTH_ACCESS_TOKEN=$(eval curl -s -X POST $OAUTH_TOKENURL $FORM_PARAMS | jq -r .access_token)
    CURL="$CURL -H \"Authorization: Bearer $OAUTH_ACCESS_TOKEN\""
else
    echo "Authenitcation with OAuth authorization type is not supported: $OAUTH_AUTHTYPE"
    exit 1
fi

export CURL
